from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import CustomUserCreationForm

from django import forms


def SignUp(request):
    if request.method == 'POST':
        f = CustomUserCreationForm(request.POST)
        if f.is_valid():
            f.save()
            messages.success(request, 'Account created successfull')
            return redirect('accounts:signup')

    else:
        f = CustomUserCreationForm()

    return render(request, 'accounts/signup.html', {'form': f})
