from django.shortcuts import render, redirect
from django.http import request,Http404
from django.views import generic
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db import transaction
from .forms import UserForm,ProfileForm
from django.contrib import messages
from django.contrib.auth.models import User

from .models import Choach, Course, CatalogCourse, Profile

def index(request):
    return render(request,'course/index.html')

class CatalogCourse(generic.ListView):
    model = CatalogCourse
    context_object_name = 'course_list'

    def get_query(self):
        return CatalogCourse.objects.all()

    template_name= 'course/catalog_course.html'

class CourseList(generic.ListView):
    model = Course

    def get_context_data(self, **kwargs):
        b = self.kwargs.get('pk')
        context = super(CourseList,self).get_context_data(**kwargs)
        context['course_list'] = Course.objects.filter(language=b).all()
        return context

    template_name= 'course/course_list.html'

class CourseDetails(LoginRequiredMixin,generic.ListView):
    model = Course
    def get_context_data(self, **kwargs):
        b = self.kwargs.get('pk')
        context = super(CourseDetails,self).get_context_data(**kwargs)
        context['course_list'] = Course.objects.filter(id=b).all()
        return context

    template_name= 'course/course_detail.html'

def login_user(request, template_name='registration/login.html',
           redirect_field_name = None,
           extra_context=None):
    return auth.views.login(request, template_name, redirect_field_name,
                            authentication_form)

class Profile(LoginRequiredMixin,generic.ListView):
    model = User
    template_name = 'course/profile_detail.html'

    def get_queryset(self):
        return User.objects.filter(username=self.request.user)


@login_required
@transaction.atomic
def update_profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, ('Your profile was successfully updated!'))
            return redirect('course:profile')
        else:
            messages.error(request, ('Please correct the error below.'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'course/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })
