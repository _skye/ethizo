from django.urls import path, include
from . import views

app_name = 'course'
urlpatterns = [
    path('',views.index,name='index'),
    path('list',views.CatalogCourse.as_view(),name='catalog_course'),
    path('detail/<int:pk>/',views.CourseList.as_view(),name='course_list'),
    path('learn/<int:pk>/',views.CourseDetails.as_view(),name='course_details'),
    path('profile/',views.Profile.as_view(),name='profile'),
    path('profile/settings/',views.update_profile,name='update_profile'),
    path('accounts/',include('django.contrib.auth.urls')),
]
