from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from datetime import date
from django.db.models.signals import post_save
from django.dispatch import receiver

class Choach(models.Model):
    full_name = models.CharField(max_length=30)

    def __str__(self):
        return self.full_name

class Language(models.Model):
    name = models.CharField(max_length=20)
    def __str__(self):
        return self.name

class CatalogCourse(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(max_length=200)
    course = models.ForeignKey(Language,related_name='course',on_delete=models.SET_NULL,null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('course:course_list', args=[str(self.id)])

class Course(models.Model):
    coach = models.ForeignKey(Choach,related_name='choach',on_delete=models.SET_NULL,null=True)
    title = models.CharField(max_length=40,null=True)
    description = models.TextField(max_length=200,null=True)
    language = models.ForeignKey(CatalogCourse,related_name='language',on_delete=models.SET_NULL,null=True)
    context = models.TextField(max_length=30000,null=True)
    video_url = models.CharField(max_length=1000,null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('course:course_details', args=[str(self.id)])

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    city = models.CharField(max_length=255)
    about = models.CharField(max_length=255,null=True)
    hobbies = models.CharField(max_length=255,null=True)
    birthdate = models.DateField(null=True, blank=True)

    def __str__(self):  # __unicode__ for Python 2
        return self.user.username

@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
